import 'dart:math';

import 'package:flutter/cupertino.dart';

class ScaleSize {
  /// Helper function to create a textScaleFactor based on the screen size
  static double textScaleFactor(BuildContext context,
      {double maxTextScaleFactor = 8}) {
    final width = MediaQuery.of(context).size.width;
    double val = (width / 1400) * maxTextScaleFactor;
    return max(1, min(val, maxTextScaleFactor));
  }
}
