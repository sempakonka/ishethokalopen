import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import 'scale_size.dart';

void main() {
  runApp(const IsHetHokAlOpen());
}

class IsHetHokAlOpen extends StatelessWidget {
  const IsHetHokAlOpen({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Is het hok al open',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const IsHetHokAlOpenPage(),
    );
  }
}

class IsHetHokAlOpenPage extends StatelessWidget {
  const IsHetHokAlOpenPage({super.key});

  final _red = const Color.fromARGB(255, 206, 52, 38);
  final _green = const Color.fromARGB(255, 131, 199, 76);

  Future<int> _getStatus() async {
    final response = await http
        .get(Uri.parse('https://beheer.syntaxis.nl/api/ishethokalopen'));

    final payload = jsonDecode(response.body)['payload'];

    if (response.statusCode != 200) {
      return Future.error("error");
    }

    return payload['open'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<int>(
        future: _getStatus(),
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return const Scaffold(
                  body: Center(child: CircularProgressIndicator()));
            default:
              if (snapshot.hasError) {
                return body(context, snapshot);
              } else {
                return body(context, snapshot);
              }
          }
        },
      ),
    );
  }

  Widget body(BuildContext context, AsyncSnapshot<int> snapshot) {
    return Scaffold(
      backgroundColor: () {
        if (snapshot.data == 1) {
          return _green;
        } else if (snapshot.data == 0) {
          return _red;
        } else {
          return Colors.black12;
        }
      }(),
      body: Center(
        child: Column(
          children: [
            Flexible(flex: 1, child: Container()),
            const Flexible(
              flex: 6,
              child: Image(
                image: AssetImage('syntaxis_white.png'),
              ),
            ),
            Flexible(
              flex: 2,
              child: Center(
                child: Text(
                  () {
                    if (snapshot.data == 1) {
                      return 'Het hok is open!';
                    } else if (snapshot.data == 0) {
                      return 'Het hok is dicht!';
                    } else {
                      return 'Er is iets mis gegaan...';
                    }
                  }(),
                  style: const TextStyle(color: Colors.white),
                  textScaleFactor: ScaleSize.textScaleFactor(context),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
